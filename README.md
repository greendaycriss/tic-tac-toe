# Tic-tac-toe game written in Python.

The program takes the user input from console and generates CPU's next move.

The player wins when the an entire line, column or diagonal is filled with O or X.


## How to Run the Game

- Run python script.py



