import random


ROWS = COLS = 3

TIC = "x"
TAC = "o"

# Init matrix with spaces
matrix = [[" " for x in range(ROWS)] for y in range(COLS)]


def print_matrix():
    print()
    for x in range(ROWS):
        print()
        print("   ", end="")

        for y in range(COLS):
            if y > 0:
                print("|", end="")
            print(" %s " % matrix[x][y], end="")

        print()
        if x < ROWS - 1:
            print("  ", end="")
            for z in range(4*ROWS):
                print("_", end="")

        print()
    print()


def generate():
    row = random.randrange(ROWS)
    col = random.randrange(COLS)

    if matrix[row][col] == " ":
        matrix[row][col] = TAC
    else:
        generate()


def completed():
    val = True
    for x in matrix:
        for y in x:
            if y == " ":
                val = False
                return val
    return val


def horizontal_win_tic():
    val = False
    for x in range(ROWS):
        for y in range(COLS):
            val = False
            if matrix[x][y] != TIC:
                break
            else:
                val = True
        if val is True:
            return val
    return val


def vertical_win_tic():
    val = False
    for x in range(ROWS):
        for y in range(COLS):
            val = False
            if matrix[y][x] != TIC:
                break
            else:
                val = True
        if val is True:
            return val
    return val


def horizontal_win_tac():
    val = False
    for x in range(ROWS):
        for y in range(COLS):
            val = False
            if matrix[x][y] != TAC:
                break
            else:
                val = True
        if val is True:
            return val
    return val


def vertical_win_tac():
    val = False
    for x in range(ROWS):
        for y in range(COLS):
            val = False
            if matrix[y][x] != TAC:
                break
            else:
                val = True
        if val is True:
            return val
    return val


def top_left_diagonal_win_tic():
    val = False
    x = y = 0
    while x < ROWS-1 and y < ROWS-1:
        if matrix[x][y] == matrix[x+1][y+1] == TIC:
            val = True
            x = x + 1
            y = y + 1
        else:
            val = False
            break
    return val


def bottom_left_diagonal_win_tic():
    val = False
    x = ROWS-1
    y = 0
    while x >= 0 and y < ROWS-1:
        if matrix[x][y] == matrix[x-1][y+1] == TIC:
            val = True
            x = x - 1
            y = y + 1
        else:
            val = False
            break
    return val


def top_left_diagonal_win_tac():
    val = False
    x = y = 0
    while x < ROWS-1 and y < ROWS-1:
        if matrix[x][y] == matrix[x+1][y+1] == TAC:
            val = True
            x = x + 1
            y = y + 1
        else:
            val = False
            break
    return val


def bottom_left_diagonal_win_tac():
    val = False
    x = ROWS - 1
    y = 0
    while x >= 0 and y < ROWS-1:
        if matrix[x][y] == matrix[x-1][y+1] == TAC:
            val = True
            x = x - 1
            y = y + 1
        else:
            val = False
            break
    return val


def play():
    while True:
        row = input("Please select row number from 1 to %s: " % ROWS)
        row = int(row)-1
        if row in range(ROWS):
            break
        else:
            print("Input out of range.")

    while True:
        col = input("Please select column number from 1 to %s: " % COLS)
        col = int(col)-1
        if col in range(COLS):
            break
        else:
            print("Input out of range.")

    if matrix[row][col] == " ":
        matrix[row][col] = TIC
        print()
        print("Your move:")
        print_matrix()

        if (not vertical_win_tic() and not horizontal_win_tic() and
                not top_left_diagonal_win_tic() and
                not bottom_left_diagonal_win_tic()):
            if not completed():
                generate()
                print("CPU's move:")
                print_matrix()

                if (vertical_win_tac() is True or
                        horizontal_win_tac() is True or
                        top_left_diagonal_win_tac() is True or
                        bottom_left_diagonal_win_tac() is True):
                    print("CPU wins!")
                    return False
            else:
                print("Game over!")
                return False
        else:
            print("You win!")
            return False
    else:
        print("Position already completed.")
        play()

    return True


print("[1] Start New Game")
print("[x] Quit")
input_value = input("Please select your choice: ")

if input_value == "1":
    print_matrix()

    bool_val = True
    while bool_val is True:
        bool_val = play()
